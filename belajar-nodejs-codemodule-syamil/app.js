const fs = require("fs");
const readLine = require("readline");
const rl = readLine.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question("Masukkan nama anda : ", (nama) => {
  rl.question("Masukkan nomor HP anda : ", (noHP) => {
    const contact = { nama, noHP };

    try {
      const fileContent = fs.readFileSync("data/contacts.json", "utf-8");
      const contacts = fileContent ? JSON.parse(fileContent) : [];

      contacts.push(contact);
      fs.writeFileSync("data/contacts.json", JSON.stringify(contacts));
      console.log("Terima kasih sudah memasukkan data.");
    } catch (err) {
      console.error("Error:", err);
    } finally {
      rl.close();
    }
  });
});
